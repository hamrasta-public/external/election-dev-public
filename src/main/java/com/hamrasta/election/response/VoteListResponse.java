package com.hamrasta.election.response;

import com.hamrasta.election.asset.Vote;
import com.hamrasta.hplatform.response.RPCResponse;
import com.hamrasta.hplatform.serializer.AssetField;

import java.util.Set;

public class VoteListResponse extends RPCResponse {
    @AssetField(index = 0, length = 32)
    private String electionId;
    @AssetField(index = 1)
    private String[] votes;
    @AssetField(index = 2, hex = false, optional = true)
    private String bookmark;

    public VoteListResponse() {
    }

    public VoteListResponse(String txId, String electionId, String[] votes, String bookmark) {
        this.txId = txId;
        this.electionId = electionId;
        this.votes = votes;
        this.bookmark = bookmark;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String[] getVotes() {
        return votes;
    }

    public void setVotes(String[] votes) {
        this.votes = votes;
    }


}
