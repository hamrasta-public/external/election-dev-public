package com.hamrasta.election.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;
import org.apache.commons.codec.binary.Hex;
import org.web3j.crypto.Hash;

import java.util.Objects;

public class Vote implements IAsset {
    @AssetField(index = 0, length = 32)
    private String electionId;
    @AssetField(index = 1, address = true)
    private String owner;
    @AssetField(index = 2)
    private String data;

    public Vote() {
    }

    public Vote(String electionId, String data, String owner) {
        this.electionId = electionId;
        this.data = data;
        this.owner = owner;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    public String getHash() {
        return Hex.encodeHexString(Hash.sha256(Hash.sha256((electionId + owner + data).getBytes())));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return Objects.equals(getHash(), vote.getHash());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHash());
    }
}
