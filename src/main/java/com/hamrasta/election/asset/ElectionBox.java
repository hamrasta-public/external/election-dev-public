package com.hamrasta.election.asset;

import com.hamrasta.hplatform.serializer.AssetField;
import com.hamrasta.hplatform.serializer.IAsset;

import java.util.HashSet;
import java.util.Set;

public class ElectionBox implements IAsset {
    @AssetField(index = 0, length = 32)
    private String id;
    @AssetField(index = 1, address = true)
    private String creator;
    @AssetField(index = 2, address = true)
    private String owner;
    @AssetField(index = 3)
    private long start;
    @AssetField(index = 4)
    private long end;
    @AssetField(index = 5, address = true)
    private String[] voters;


    public ElectionBox() {
    }


    public ElectionBox(String creator, String owner, long start, long end, String id, String[] voters) {
        this.start = start;
        this.end = end;
        this.owner = owner;
        this.id = id;
        this.voters = voters;
        this.creator = creator;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getVoters() {
        return voters;
    }

    public void setVoters(String[] voters) {
        this.voters = voters;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
