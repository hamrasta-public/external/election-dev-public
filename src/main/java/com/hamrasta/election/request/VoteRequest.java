package com.hamrasta.election.request;

import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import org.web3j.crypto.Sign;

public class VoteRequest extends RPCRequest {
    @AssetField(index = 0, length = 32)
    private String electionId;
    @AssetField(index = 1)
    private long time;
    @AssetField(index = 2)
    private String data;

    public VoteRequest() {
    }

    public VoteRequest(String electionId, long time, String data) {
        this.electionId = electionId;
        this.data = data;
        this.time = time;
    }

    public VoteRequest(long nonce) {
        super(nonce);
    }
    public VoteRequest(long nonce, Sign.SignatureData[] signatureData) {
        super(signatureData, nonce);
    }
    
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
