package com.hamrasta.election.request;

import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import org.web3j.crypto.Sign;

import java.util.HashSet;
import java.util.Set;

public class CreateRequest extends RPCRequest {
    @AssetField(index = 0, length = 32)
    private String electionId;
    @AssetField(index = 1, address = true)
    private String owner;
    @AssetField(index = 2)
    private long start;
    @AssetField(index = 3)
    private long end;
    @AssetField(index = 4, address = true)
    private String[] voters;

    public CreateRequest() {
    }

    public CreateRequest(String electionId, String owner, long start, long end, String[] voters) {
        this.electionId = electionId;
        this.owner = owner;
        this.start = start;
        this.end = end;
        this.voters = voters;
    }


    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public String[] getVoters() {
        return voters;
    }

    public void setVoters(String[] voters) {
        this.voters = voters;
    }
}
