package com.hamrasta.election.request;

import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;

import java.util.HashSet;
import java.util.Set;

public class ChangeRequest extends RPCRequest {
    @AssetField(index = 0, length = 32)
    private String electionId;
    @AssetField(index = 1, address = true)
    private String owner;
    @AssetField(index = 2)
    private long start;
    @AssetField(index = 3)
    private long end;
    @AssetField(index = 4, address = true)
    private String[] deleteVoters;
    @AssetField(index = 5, address = true)
    private String[] addVoters;

    public ChangeRequest() {
    }

    public ChangeRequest(String electionId, String owner, long start, long end, String[] deleteVoters, String[] addVoters) {
        this.electionId = electionId;
        this.owner = owner;
        this.start = start;
        this.end = end;
        this.deleteVoters = deleteVoters;
        this.addVoters = addVoters;
    }


    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public String[] getDeleteVoters() {
        return deleteVoters;
    }

    public void setDeleteVoters(String[] deleteVoters) {
        this.deleteVoters = deleteVoters;
    }

    public String[] getAddVoters() {
        return addVoters;
    }

    public void setAddVoters(String[] addVoters) {
        this.addVoters = addVoters;
    }
}
