package com.hamrasta.election.request;

import com.hamrasta.hplatform.request.RPCRequest;
import com.hamrasta.hplatform.serializer.AssetField;
import org.web3j.crypto.Sign;

public class ResultRequest extends RPCRequest {
    @AssetField(index = 0, length = 32)
    private String electionId;
    @AssetField(index = 1, hex = false, optional = true)
    private String bookmark;

    public ResultRequest() {
    }

    public ResultRequest(String electionId, String bookmark) {
        this.electionId = electionId;
        this.bookmark = bookmark;
    }

    public ResultRequest(long nonce) {
        super(nonce);
    }
    public ResultRequest(long nonce, Sign.SignatureData[] signatureData) {
        super(signatureData, nonce);
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }


}
